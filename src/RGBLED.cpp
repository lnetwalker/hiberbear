/*
 * RGBLED.cpp
 * (c) 2018 by Hartmut Eilers <hartmut@eilers.net>
 * distributed under the terms of the GNU GPL V2.0
 */


/**
 * Notes on the mood feature 
 * 
 * if you look at what is happening when you fade through the colors,
 * you see 6 Phases:
 * 
 * Phase1: R=0xff
 *         G=0x0 -> 0xff
 *         B=0x0
 * Phase2: R=0xff -> 0x0       
 *         G=0xff
 *         B=0x0
 * Phase3: R=0x0         
 *         G=0xff
 *         B=0x0 -> 0xff
 * Phase4: R=0x0
 *         G=0xff -> 0x0
 *         B=0xff
 * Phase5: R=0x0 -> 0xff         
 *         G=0x0
 *         B=0xff
 * Phase6: R=0xff         
 *         G=0x0
 *         B=0xff -> 0x0
 *         
 * you need a resolution ( number of steps ) you use to fade through the colors. e.g. 200 steps         
 * PhaseLength=resolution / 6                          // we have 6 Phases
 * eg Resolution=200 -> PhaseLength=200/6=33
 * that means that every Phase is 33 steps ( or 33 color changes per step )
 * FadeDiff = 256 / PhaseLength
 * FadeDiff is the delta for every change in the color
 * 
 * To get the same result with this new solution compared to the old one, we use a resolution of 94
 *  
 *
 */
 
#include "RGBLED.h"
#include <Arduino.h>

 RGBLED::RGBLED(int resolution, int mooddelay, int RPin, int GPin, int BPin){
  /**
   * constructor for common cathode RGB LEDs
   */
  _RPin = RPin;
  _GPin = GPin;
  _BPin = BPin;

  // Set GPIOs to output
  pinMode(RPin,OUTPUT);
  pinMode(GPin,OUTPUT);
  pinMode(BPin,OUTPUT);

  _mooddelay=mooddelay;
  _moodcnt=_mooddelay;
  _fader=0;
  _MoodEnable = true;
  _blinkIt = 0;
  _BlinkOn = true;
  _resolution = resolution;
  _PhaseLength = int( _resolution / 6 );
  _FadeDiff = int(256 / ( _resolution / 6 ));
  _currR = 0xff;
  _currG = 0;
  _currB = 0;
 }

 int RGBLED::setAlarm(int R, int G, int B) {
  /**
   * disrupts the mood led and show a blinking color
   */
  disableMood();
  setBlink(R,G,B);
 }

 int RGBLED::enableMood() {
  /**
   * enable color fading
   */
  _MoodEnable = true;
 }

 int RGBLED::disableMood() {
  /**
   * disable the mood function ( no LED color fading )
   */
  _MoodEnable = false;
 }

 int RGBLED::setBlink(int R, int G, int B) {
  /**
   * let the LED blink ( used in setAlarm to raise the attention )
   */
  if ( _blinkIt == 0 ) {
    _blinkIt = _BlinkIntervall;
    if ( _BlinkOn ) {
      _BlinkOn = false;
      setColor(R,G,B);
    } else {
      _BlinkOn = true;
      setColor(0,0,0);
    }
  } else {
    _blinkIt--;
  }
 }
 
 int RGBLED::setColor(int R, int G, int B){
  /**
   * set the RGB LED to the specified color
   */
  _currR = R;
  _currG = G;
  _currB = B;
  #ifdef ESP8266
  analogWrite(_RPin, R);
  analogWrite(_GPin, G);
  analogWrite(_BPin, B);
  #endif
 }

 int RGBLED::fadeColor(){
  /**
   * fade the color through all possible values 
   */
   
  if ( _MoodEnable ) {                                // only fade the RGB if it is enabled 
    if (_moodcnt>0 ) {
      _moodcnt=_moodcnt -1;                           
    } else {                                          // next color
      _moodcnt = _mooddelay;
      Serial.print("fader: ");
      Serial.println(_fader);
      _Phase = int ( _fader / _PhaseLength )+1;
      Serial.print("Phase: ");
      Serial.println(_Phase);
      if ( _Phase == 1 ) {                            // change color depending on phase
        setColor(_currR,_currG+_FadeDiff,_currB);
      }
      if ( _Phase == 2 ) {
        setColor(_currR-_FadeDiff,_currG,_currB);
      }
      if ( _Phase == 3 ) {
        setColor(_currR,_currG,_currB+_FadeDiff);
      }
      if ( _Phase == 4 ) {
        setColor(_currR,_currG-_FadeDiff,_currB);
      }
      if ( _Phase == 5 ) {
        setColor(_currR+_FadeDiff,_currG,_currB);
      }
      if ( _Phase == 6 ) {
        setColor(_currR,_currG,_currB-_FadeDiff);
      }
      _fader= _fader+1;
      if ( _fader >= _resolution ) {                   // are we at the end ?
        _fader = 0;                                   // restart from beginning
      }
    }
  }
}  
