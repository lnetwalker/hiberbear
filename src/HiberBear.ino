/*
 * Project HiberBear
 * (c) 2018 by Hartmut Eilers <hartmut@eilers.net>
 */


#undef DEBUG_SERIAL

#include <Arduino.h>                                                // Standard include for all Arduino projects
#include <Wire.h>                                                   // I2C library
#include <radio.h>                                                  // Radio module
#include <RDA5807M.h>                                               // Radio chipset
#include <RDSParser.h>
#include <NTPClient.h>                                              // get realtime via NTP
#include <WiFiUdp.h>
#include <oled.h>                                                   // driver for OLED Display
#define OLEDdisplay                                                 // used by AnalogClock to determine display
#undef Digoledisplay
#define smalldisplay
#include <AnalogClock.h>                                            // class for an analog clock
#include "BME280.h"                                                 // BME 280 Sensor
#include <Ticker.h>                                                 // Ticker Library ( for timetriggers )
#include "Ai_AP3216_AmbientLightAndProximity.h"                     // AP3216 proximity sensor
#define NEARENOUGH 10                                               // the value that triggers display on
#include "FS.h"
#include "SPIFFS.h"

#include <WiFi.h>                                                   // Wifi Network
#include <WebServer.h>                                              // Webserver stuff

#include <PubSubClient.h>                                           // MQTT client library


#include "AudioTools.h"                                             // I2S Audio Library
#include "AudioCodecs/CodecMP3Helix.h"                              // MP3 decoder for URL streams
#include "AudioLibs/AudioKit.h"                                     // access for LyraT

// Wifi settings
const char *SSID = "hucky.net";                                           // insert your SSID
const char *PASS = "internet4hucky";                                      // insert your password
//WiFiClient HBClient;                                                // HBClient = HiberBear Client

// Radio Settings
#define FIX_BAND     RADIO_BAND_FM                                  // The band that will be tuned by this sketch is FM.
#define FIX_STATION  9240                                           // The station that will be tuned by this sketch is 100.80 MHz.
#define FIX_VOLUME   14                                             // The volume that will be set by this sketch is level 4.
int FIX_VOLUME_LEVELS=15;                                           // the volume can be set in 15 levels
int VOL_DIAL_RESOLUTION= 1024;                                      // resolution of the volume A/D
int vol_scaler=int(VOL_DIAL_RESOLUTION/FIX_VOLUME_LEVELS);          // the scaling factor to calcthe volume setting
RDA5807M radio;                                                     // Create an instance of Class for RDA5807M Chip
RDSParser rds;                                                      // get a RDS parser
String Station;                                                     // the tuned radio station name
String receivedRDS;                                                 // the received RDS text
String RDSstring;
String DisplayText;
int RDSPointer,RDSScroll;
#define SCROLLTIME 10

// I2S Stream Handling
AudioKitStream kit;                                                 // access to I2S as stream
StreamCopy copier(kit,kit);                                         // Line in to speaker out
#define FM_RADIO 0
#define URL_RADIO 1
int HBAudioInput;
// Internet Radio stations
#define MP3StreamBufferSize 4096                                    // size of the buffer used for mp3 streaming
const char *urls[] = {
  "https://dispatcher.rndfnk.com/br/br1/schwaben/mp3/mid",
  "https://dispatcher.rndfnk.com/swr/swr1/bw/mp3/128/stream.mp3",
  "https://tunein.com/radio/SWR3-996-s24896/",
  "https://www.radio.de/s/bigfmreggaevibez",
  "http://sunshineradio.ice.infomaniak.ch/sunshineradio-128.mp3",
  "http://stream.srg-ssr.ch/m/rsj/mp3_128",
  "http://stream.srg-ssr.ch/m/drs3/mp3_128",
  "http://stream.srg-ssr.ch/m/rr/mp3_128",
  "http://streaming.swisstxt.ch/m/drsvirus/mp3_128"
};
       
ICYStream urlStream(SSID,PASS,MP3StreamBufferSize);
//ICYStream urlStream(SSID,PASS);
AudioSourceURL source(urlStream, urls, "audio/mp3");
MP3DecoderHelix decoder;
AudioPlayer player(source, kit, decoder);
String currentTitle;                                                // the currently played title
float Volume;

// I2C
#define I2C_SCL 23                                                  // I2C Setup: use GPIO 18 and 23 for SDA and SCL
#define I2C_SDA 18                                                  // these are on I2C header on LyraT board

// Display
OledDisp myOLED(0,128,64),*P;                                       // define new Display 
                                                                    // offset=2 for SH1106
                                                                    // offset=0 for SDD1306
// generel display settings                                                                    
#define DISPLAYTIMEOUT 300                                          // switch off display after this amount of seconds
Ticker SleepDisplay;                                                // prepare the ticker for dispay timeout
int oledUpdateIntervall = 5000;                                     // update screen every 5000 ms
unsigned long oledUpdateTimeout;                                    // time in ms when update is needed
// screen related varibles
#define ACTIVETIME 30000                                            // 30s a screen is active
#define SCREENMAX 4                                                 // number of screens
unsigned long DisplayActiveTimeout;                                 // ms when a display switch is scheduled
int Screen;                                                         // the number of the active screen which is displayed
// EasyScreenBlank settings
#define PAUSEMS 150                                                 // Pause in ms in EasyScreenBlank
unsigned long EasyScreenBlankTimeout;                               // time in millis when scrennblank will act
int EasyScreenBlankCnt = 0;                                         // counter which line to delete
boolean EasyScreenBlankDone = true;                                 // signal everything done, nothing to do
// EasyDisplayBuffer handling
boolean EasyDisplayBufferDone = true;
int EasyDisplayBufferCnt = 0;
unsigned long EasyDisplayBufferTimeout;
boolean IotDisplaySelect;                                           // true = show temp, false = show pressure

// time/clock related settings
WiFiUDP ntpUDP;
NTPClient timeClient(ntpUDP);
#define TIMEZONE 1                                                  // timediff to UTC
int clockCenterX=31;
int clockCenterY=31;
int radius=30;
int old_hour,old_minute;
AnalogClock HiberbearClock = AnalogClock();                                                                    
const int MaxX = 15;                                                // define physical screen width
const int MaxY = 7;

BME280 bme280;                                                      // temp/hum sensor

Ai_AP3216_AmbientLightAndProximity aps = Ai_AP3216_AmbientLightAndProximity(); // new proximity sensor


void printMetaData(MetaDataType type, const char* str, int len){    // Read Audio Metadata
  if ( toStr(type) == "Name" ) {
    Station = str;
  }
  if ( toStr(type) == "Title" ) {
    currentTitle = str;
    currentTitle = currentTitle.substring(0,44);                         // only use the max first 44 chars
    Serial.print("TempStr=");
    Serial.println(currentTitle);
  }
  Serial.print("==> ");
  Serial.print(toStr(type));
  Serial.print(": ");
  Serial.println(str);

}

void DisplayServiceName(char *Stationname)                         // get the Stationname text 
{
  Serial.print("RDS:"); Serial.println(Stationname);
  if ( strlen(Stationname) > 0 ) {
    Station = Stationname;
  }
}

void DisplayRDSText(char *RDStext)                                 // receive the additional RDS Text Info
{
  Serial.print("RDS Info: "); Serial.println(RDStext);
  if ( strlen(RDStext) > 0 ) {
    receivedRDS = RDStext;
    myOLED.setFont(2);                                                   // use the alternate font to display the RDS activity 
    int X=7;
    int Y=13;
    myOLED.sendStrXY("&",X,Y);
    delay(50);
    myOLED.sendStrXY(" ",X,Y);
    delay(50);
    myOLED.sendStrXY("&",X,Y);
    delay(25);
    myOLED.sendStrXY(" ",X,Y);
    myOLED.setFont(1);
  }   
}

void RDS_process(uint16_t block1, uint16_t block2, uint16_t block3, uint16_t block4) {
  rds.processData(block1, block2, block3, block4);
}

void easyScreenBlank() {
  /*
   * some notes on blanking the screen:
   * if I just use the myOLED.clear_display routine, there is so much traffic on I2C
   * that the I2S audio stream has hearable glitches. What happens there is that
   * 1KB of data is transferred as fast as possible over I2C. Obviously this is the reason
   * for the hearable glitch. To avoid that, I have written this routine which deletes
   * one line, does nothing for PAUSELOOPS iterations of loop(), then deletes the next line,
   * "sleeps" for PAUSELOOPS loop() iterations and so on, until all 8 lines are deleted
   * I won't expect these values working on the much slower ESP8266
   */
  if ( !EasyScreenBlankDone ) {                                     // start the blanking by switching off the display
    if (EasyScreenBlankCnt == 0 ) {
      myOLED.displayOff();
      EasyScreenBlankTimeout = millis() + PAUSEMS;
      EasyScreenBlankCnt++;                                         // next line to delete
    }
  
    if ( millis() >= EasyScreenBlankTimeout ) {                     // wait PAUSEMS ms
      myOLED.setXY(EasyScreenBlankCnt - 1,0);                       // delete one line
      for(int i=0;i<(132);i++)                                      // locate all COL
      {
        myOLED.sendChar(0);                                         // clear all COL
      }
      EasyScreenBlankCnt++;                                         // next line to delete
      EasyScreenBlankTimeout = millis() + PAUSEMS;                  // schedule time to run next
    }

    if (EasyScreenBlankCnt == 9 ) {                                 // all lines deleted -> swith on Display
      myOLED.displayOn();
      EasyScreenBlankCnt = 0;                                       // prepare next delete screen
      EasyScreenBlankDone = true;                                   // signal done
      oledUpdateTimeout = millis()+200;                             // schedule a oled update in 200ms
    }
  }  
}

void easyDisplayBuffer() {                                          // write the contents of the display buffer to the display
                                                                    // do it very polite by making a pause, not to block i2s
  if ( !EasyDisplayBufferDone ) {                                   // as long as there are unprocessed lines                                 
    if ( EasyDisplayBufferCnt == 0 ) {                              // the start of the display framebuffer update
      EasyDisplayBufferTimeout = millis() + PAUSEMS;                // schedule next update
      EasyDisplayBufferCnt++;
    } 

    if ( millis() >= EasyDisplayBufferTimeout ) {                   // update one row
      myOLED.setXY(EasyDisplayBufferCnt - 1,0);                     // select the row to process
      for ( int col=0;col<128; col++ ) { 			                      // loop over 128 cols in a ROW
        byte pixelbyte=0;
	      for ( int pixelbit=0; pixelbit<8;pixelbit++) {		          // loop over the 8 graph lines of a textrow
          if (myOLED.fbuffer[( EasyDisplayBufferCnt -1 )*1024+pixelbit*128+col]) {
            pixelbyte=pixelbyte+pow(2,pixelbit);
          }
        }
        myOLED.sendChar(pixelbyte);                                 // update the column
      }
      EasyDisplayBufferCnt++;                                       // next row
      EasyDisplayBufferTimeout = millis() + PAUSEMS;                // schedule next update
    }

    if ( EasyDisplayBufferCnt == 9 ) {                              // we are done, all lines processed -> cleanup
      EasyDisplayBufferCnt = 0;
      EasyDisplayBufferDone = true;
      oledUpdateTimeout = millis() + 200;                           // schedule a oled update in 200ms  
    }
  }  
}

void ftoa(double val, char *buf) {                                  // convert double to C String
  itoa((int)val, buf, 10);
  int i = strlen(buf);
  buf[i++] = '.';
  val = (int)(val * 10) % 10;
  itoa(val, &buf[i], 10);
}

int DisplaySwitcher(int activeScreen) {                             // returns the number of the active screen
  if ( millis() >= DisplayActiveTimeout ) {
    EasyScreenBlankDone = false;                                    // delete the screen very polite
    easyScreenBlank();
    DisplayActiveTimeout = millis() + ACTIVETIME;                   // schedule next display switch
    activeScreen++;
    oledUpdateTimeout = millis();                                   // set immidiate oled update !
    if ( activeScreen > SCREENMAX ) {
      activeScreen = 1;
    }
  }
  return(activeScreen);
}

void DisplayTimeout () {                                            // switch off Display after timeout
  myOLED.displayOff();        
  SleepDisplay.detach();                                            // detach from timer
}

void DisplayActive () {                                             // switch on Display when object is near
  myOLED.displayOn();
  SleepDisplay.attach(300, DisplayTimeout);                         // attach the timer to switch it off after timeout
}

void logo() {
  /**
   * load a logo from SPIFFS FS and display that
   */
  if (!SPIFFS.begin()) {
    Serial.println("Err mounting FS");
    return;
  }
  File logofile=SPIFFS.open("/HiberBear-Logo.bits","r");
  int linenumber;
  if (!logofile)  {
    Serial.println("failed to open file /HiberBear-Logo.bits for reading");
    return;
  } else {                                                          // read the file
    linenumber = 0;     
    while(logofile.available()) {
      String line=logofile.readStringUntil('\n');                   // read a line
      linenumber++;                                                 // note the linenumber because its the y coordinate of the pixel
      for (int i=0;i<line.length();i++) {                           // loop char by char over the line
        if ( line.substring(i,i+1) == "1" ) {                       // every one means set a pixel
          myOLED.setPixel(i+48,linenumber+16);
        }
      }
    }
    myOLED.displayBuffer();                                         // display the image on screen
    myOLED.sendStrXY("HiberBear boot..",7,0);
  }
}

void next(bool, int, void*) {
   player.next();
}

void previous(bool, int, void*) {
   player.previous();
}

void stopResume(bool, int, void*){
  if (player.isActive()){
    player.stop();
  } else{
    player.play();
  }
}

void voldwn(bool, int, void*) {
  Volume = Volume - 0.05;
  if ( Volume < 0 ) {
    Volume = 0;
  }
  player.setVolume(Volume);
}

void volup(bool, int, void*) {
  Volume = Volume + 0.05;
  if ( Volume > 1.0 ) {
    Volume = 1.0;
  }
  player.setVolume(Volume);
}

void SelectAudioSrc(int AudioSrc) {
  // end any current audio stream config
  Volume=0.8;
  if ( AudioSrc == FM_RADIO ) {
    auto cfg = kit.defaultConfig(RXTX_MODE);
    cfg.sd_active = false;
    cfg.input_device = AUDIO_HAL_ADC_INPUT_LINE2;                   // aux input
    kit.begin(cfg);
  } 
  if ( AudioSrc == URL_RADIO ) {
    auto cfg = kit.defaultConfig(TX_MODE);
    cfg.sd_active = false;
    kit.begin(cfg);

    // setup player
    player.setVolume(Volume);
    player.setMetadataCallback(printMetaData);
    player.begin();
  }
  /*                                                              // setup navigation
   * LyraT v 4.3 board buttons
   * #define PIN_KEY1 BUTTON_REC_ID
   * #define PIN_KEY2 BUTTON_MODE_ID
   * #define PIN_KEY3 BUTTON_PLAY_ID
   * #define PIN_KEY4 BUTTON_SET_ID
   * #define PIN_KEY5 BUTTON_VOLDOWN_ID
   * #define PIN_KEY6 BUTTON_VOLUP_ID
   */
  kit.addAction(PIN_KEY4, next);                                  // touch "Set"
  kit.addAction(PIN_KEY3, previous);                              // touch "Play"
  kit.addAction(PIN_KEY5, voldwn);                                // touch "Vol-"
  kit.addAction(PIN_KEY6, volup );                                // touch "Vol+"
}


void setup() {
  Serial.begin(115000);                                             // open the Serial port
  Serial.println("HiberBear starting...");

  Wire.begin(I2C_SDA, I2C_SCL);                                     // Initialize I2C 

  // display related setup
  myOLED.init_OLED();                                               // init display                            
  myOLED.reset_display();
  logo();
  delay(1000);
  
  SleepDisplay.attach(DISPLAYTIMEOUT, DisplayTimeout);              // switch off display after 5 minutes
  aps.begin();                                                      // activate proximity sensor
  aps.startAmbientLightAndProximitySensor ();

  // Radio setup
  radio.init();                                                     // Initialize the Radio 
  radio.debugEnable();                                              // Enable information to the Serial port
  radio.setBandFrequency(FIX_BAND, FIX_STATION);                    // Set all radio setting to the fixed values.
  radio.setVolume(FIX_VOLUME);
  radio.setMono(false);
  radio.setMute(false);
  radio.setBassBoost(true);
  RDSPointer=0;
  RDSScroll=0;
  radio.attachReceiveRDS(RDS_process);                              // setup the information chain for RDS data.       
  rds.attachServicenNameCallback(DisplayServiceName);
  rds.attachTextCallback(DisplayRDSText);

  // I2S setup
  //#ifdef DEBUG_SERIAL
  AudioLogger::instance().begin(Serial, AudioLogger::Warning);
  //#endif
  HBAudioInput = FM_RADIO;
  HBAudioInput = URL_RADIO;
  SelectAudioSrc(HBAudioInput);

  // Time / Clock realted setup
  timeClient.begin();                                               // Time
  old_hour=0;
  old_minute=0;
  P=&myOLED;
  HiberbearClock.begin(P,clockCenterX,clockCenterY,radius);

  delay(2000);                                                      // show the logo for 2 seconds ;)

  // show network settings on startup
  myOLED.clear_display();
  myOLED.sendStrXY("SSID :" ,0,0);  myOLED.sendStrXY(SSID,0,7);     // prints SSID on OLED
  char result[16];
  sprintf(result, "%3d.%3d.%3d.%3d", WiFi.localIP()[0], WiFi.localIP()[1], WiFi.localIP()[2], WiFi.localIP()[3]);
  myOLED.sendStrXY(result,2,0);
  delay(2000);
  myOLED.clear_display();

  // IoT sensor init
  bme280.begin();                                                   // init bme280

  EasyScreenBlankCnt = 0;                                           // start delete screen at first row
  EasyScreenBlankDone = true;                                       // no screen blank active
  oledUpdateTimeout = millis();                                     // set immidiate oled update !
  Screen = 2;                                                       // select screen 2 as first screen ( radio info )
  DisplayActiveTimeout = millis() + ACTIVETIME;                     // set time for next display switch
  IotDisplaySelect = true;                                          // select temp to show
}

void loop() {

  // I2S Sound stream handling
  switch ( HBAudioInput ) {
  case FM_RADIO:
    // from ADC to DAC
    copier.copy();
    break;

  case URL_RADIO:
    // URL stream
    player.copy();
    break;
  }

  // process button presses
  kit.processActions();

  easyScreenBlank();                                                // take care of deleting the screen without interrupting I2S
  easyDisplayBuffer();                                              // handle needed framebuffer updates

  /*
  char s[12];                                                       // print radio debug info on serial port
  radio.formatFrequency(s, sizeof(s));
  Serial.print("Station:"); 
  Serial.println(s);
  
  Serial.print("Radio:"); 
  radio.debugRadioInfo(); 
  Serial.print("Audio:"); 
  radio.debugAudioInfo();
  */

  // Time
  timeClient.update();                                              // keep time running...

  long alsValue = aps.getAmbientLight();                            // read the proximity sensor
  long psValue = aps.getProximity();              
  //Serial.print("ALS ");Serial.println(alsValue);
  //Serial.print("PS  ");Serial.println(psValue);
  if ( psValue > NEARENOUGH ) {                                     // switch on display if object is near
    DisplayActive ();
  }

  radio.checkRDS();                                                 // check for RDS data

  Screen = DisplaySwitcher(Screen);                                 // select the active screen
  //Screen = 1; //DEBUG

  if ( millis() >= oledUpdateTimeout ) {
    oledUpdateTimeout = millis() + oledUpdateIntervall;             // schedule next update

    // get the time
    int Hours = timeClient.getHours()+TIMEZONE;                       // hours is in UTC, so add some hours to adapt timezone
    if ( Hours >= 24 ) { Hours=Hours - 24 ; }
    String hoursStr = Hours < 10 ? " " + String(Hours) : String(Hours);
    int Minutes = timeClient.getMinutes();
    String minuteStr = Minutes < 10 ? "0" + String(Minutes) : String(Minutes);
    String Zeit=hoursStr + ":" + minuteStr;

    // get the IoT data
    char tempbuf[12];                                               // Iot sensor data bme280
    char humidbuf[12];
    char pressurebuf[12];
    double temp = 0.0, pressure = 0.0, humid=0.0;
 
    temp = bme280.readTemperature();
    humid = bme280.readHumidity();
    pressure = bme280.readPressure();

    ftoa(temp, tempbuf);
    ftoa(humid, humidbuf);
    ftoa(pressure, pressurebuf);

    switch (Screen) {
    case 1:                                                         // IoT Sensor data
      myOLED.sendBigStrXY(Zeit.c_str(),1,1);                        // print time on OLED
      if ( IotDisplaySelect ) {                                     // print temp on OLED
        myOLED.sendBigStrXY(tempbuf,5,1);
        myOLED.sendStrXY(" ",5,13);
        myOLED.setFont(2);
        myOLED.sendStrXY("%",5,14);
        myOLED.setFont(1);
        myOLED.sendStrXY("C",5,15);
        IotDisplaySelect = false;
      } else {                                                      // print pressure on OLED
        myOLED.sendBigStrXY(pressurebuf,5,1);
        myOLED.sendStrXY("hPa",5,13); 
        IotDisplaySelect = true;
      }
      break;
  
    case 2:                                                         // print radio station data
      myOLED.sendStrXY("you're listening" ,0,0);  
      myOLED.sendStrXY(Station.c_str(),2,1);                        // station name
      //myOLED.sendBigStrXY(Zeit.c_str(),6,3);                        // print time on OLED
      if ( HBAudioInput == FM_RADIO ){
        if ( RDSScroll > SCROLLTIME ) {
          RDSstring=receivedRDS.c_str();
          if ( RDSstring.length() > 15) {
            DisplayText=RDSstring.substring(RDSPointer,RDSPointer+15);
            myOLED.sendStrXY(DisplayText.c_str(),6,1);                // print RDS Text
            RDSPointer++;
            if (RDSPointer>RDSstring.length()-15) {
              RDSPointer=0;
            }
          } else {
            myOLED.sendStrXY(receivedRDS.c_str(),6,1);
          }
          RDSScroll=0;
        } else {
          RDSScroll++;
        }
      } else {
        if ( HBAudioInput == URL_RADIO ) {
          //Serial.print("Screen2 -> CurrentTile=");
          //Serial.println(currentTitle);
          myOLED.sendStrXY(currentTitle.c_str(),4,1);
        }
      }

      break;
    
    case 3:
      myOLED.sendBigStrXY(Zeit.c_str(),3,3);                        // print big time on OLED
      break;
    
    case 4:                                                         // draw analog clock
      if ( old_minute != Minutes ) { 
        HiberbearClock.drawDisplay();
        //drawSec(t.sec);
        HiberbearClock.drawMin(Minutes);
        HiberbearClock.drawHour(Hours, Minutes);
        // polite update of the oled frambuffer to avoid dissruptions the audio stream ( see easyScreenBlank(); )
        EasyDisplayBufferDone = false;
        EasyDisplayBufferCnt = 0;
        easyDisplayBuffer();
        old_minute=Minutes;
      }
      myOLED.sendStrXY(pressurebuf,0,7);                            // print pressure on OLED
      myOLED.sendStrXY("hPa",0,13);
      myOLED.sendStrXY(tempbuf,4,9);                                // print temp on OLED
      myOLED.setFont(2);
      myOLED.sendStrXY("%",4,13);
      myOLED.setFont(1);
      myOLED.sendStrXY("C",4,14);
      //myOLED.sendStrXY(Station.c_str(),7,7);                        // station name
      break;
    }
  }
    //myOLED.heartbeat(7,14);                                           // show alive sign
}
