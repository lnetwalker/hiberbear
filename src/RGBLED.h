/*
 * RGBLED.h
 * (c) 2018 by Hartmut Eilers <hartmut@eilers.net>
 */

#ifndef RGBLED_h
#define RGBLED_h

class RGBLED {
  public:
    RGBLED(int resolution ,int mooddelay, int RPin, int GPin, int BPin);
    int setBlink(int R, int G, int B);
    int setColor(int R, int G, int B);
    int fadeColor();
    int enableMood();
    int disableMood();
    int setAlarm(int R, int G, int B);
    const int red [3] = {255,0,0};
    const int green [3] = {0,255,0};
    const int yellow [3] = {255,255,0};
    const int blue [3] = {0,0,255};
  private:
    int _resolution;
    int _PhaseLength;
    int _FadeDiff;
    int _Phase;
    bool _MoodEnable;
    int _RPin;
    int _GPin;
    int _BPin;
    int _currR;
    int _currG;
    int _currB;
    const int _BlinkIntervall = 10;
    int _blinkIt;
    bool _BlinkOn;

    int _moodcnt;
    int _mooddelay;
    int _fader;
};

#endif
